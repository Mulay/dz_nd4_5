const http = require("http");
const querystring = require('querystring');

let data = JSON.stringify({
    'firstName' : 'oleg',
    'lastName' : 'mulko'
});

console.log(data);

let options = {
    hostname: 'localhost',
    port: 3000,
    path: '/key',
    method: 'POST',
    headers: {
        'Content-Type' : 'application/json'
    }
};

let request = http.request(options, (res) => {
    console.log(`Статус: ${res.statusCode}`);
    console.log(`Заголовки: ${JSON.stringify(res.headers)}`);
    res.setEncoding('utf8');
    let data = "";
    res.on('data', (chunk) => {
        data += chunk;
        console.log(`Тело ответа: ${chunk}`);
    });
    res.on('end', () => {
        console.log('Данные получены!');
        console.log(JSON.parse(data));
        process.close;
    });
});

request.on('error', (err) => {
    console.log(`problem with request: ${err.message}`);
    console.log(err);
});

request.write(data);
request.end();
