
const http = require("http");
const server = http.createServer();
const port = process.env.PORT || "3000";
const handler = require("handler");

server
    .listen(port)
    .on('error', err => console.error(err))
    .on('request', handler)
    .on('listening', () => {
        console.log('Start HTTP on port %d', port);
    });